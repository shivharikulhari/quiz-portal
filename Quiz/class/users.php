<?php
session_start();
class users{
	public $host="localhost";
	public $username="root";
	public $pass="";
	public $db_name="quiz";
	public $conn;
	public $data;
	public $cat;
	public $qus;
	
	public function __construct()
	{
		$this->conn=new mysqli($this->host,$this->username,$this->pass,$this->db_name);
		if($this->conn->connect_errno)
		{
			die ("database connection failed".$this->conn->connect_errno);
		}
	}
	
	public function signup($data)
	{
		$this->conn->query($data);
		return true;
	}

	public function store_result($id, $name, $right, $total){
		$time = date("Y m d h:i:s",time());
		$this->conn->query("insert into score (id, name, correct, total, time, cat)
			VALUES ('$id','$name','$right','$total', '$time', '$_SESSION[cat]'  )");
	}
	
	public function signin($email,$pass)
	{
		$query=$this->conn->query("select email,pass,isTeacher from users where email='$email' and pass='$pass'");
	    $row = $query->fetch_array(MYSQLI_ASSOC);
		if($query->num_rows>0)
		{
			$_SESSION['email']=$email;
			$_SESSION['teacher'] = $row['isTeacher'];
			return true;
		}
		else
		{
			return false;
		}
	
	}

	public function	remove_question($id){
		$q = $this->conn->query("DELETE FROM questions WHERE id = $id");
	}

	public function get_results(){
		$q = $this->conn->query("SELECT * FROM score ORDER BY cat ASC, id ASC");
		$res = array();
		while ($row = $q->fetch_array(MYSQLI_ASSOC)){
			array_push($res, $row);
		}
		return $res;
	}

	public function getQuestion(){
		$q = $this->conn->query("SELECT * FROM questions");
		$res = array();
		while ($row = $q->fetch_array(MYSQLI_ASSOC)){
			array_push($res, $row);
		}
		return $res;
	}

	public function	users_profile($email)
	{
		$query=$this->conn->query("select * from users where email='$email'");
	    $row=$query->fetch_array(MYSQLI_ASSOC);
		if($query->num_rows>0)
		{
			$this->data[]=$row;
		}
		return $this->data;
	}
	public function cat_shows()
	{		
		$query=$this->conn->query("select * from category");
	   while($row=$query->fetch_array(MYSQLI_ASSOC))		
		{
			
			$this->cat[]=$row;
		}
		return $this->cat;
	
	}
	public function qus_show($qus)
	{
		//echo $qus;
		 $query=$this->conn->query("select * from questions where cat_id='$qus'");
	    while($row=$query->fetch_array(MYSQLI_ASSOC))		
		{			
			$this->qus[]=$row;
		}
		return $this->qus; 
	}
	public function answer($data)
	{
		 $ans=implode("",$data);
		 $right=0;
		 $wrong=0;
		 $no_answer=0;
		 $query=$this->conn->query("select id,ans from questions where cat_id='".$_SESSION['cat']."'");
	    while($qust=$query->fetch_array(MYSQLI_ASSOC))		
		{			
			if($qust['ans']==$_POST[$qust['id']])
			{
				 $right++;
			}
			elseif($_POST[$qust['id']]=="no_attempt")
			{
				 $no_answer++;
			}
			else
			{
				$wrong++;
			}
		}
		$array=array();
		$array['right']=$right;
		$array['wrong']=$wrong;
		$array['no_answer']=$no_answer;
		return $array;
		
	}
	public function add_quiz($rec)
	{
		$a=$this->conn->query($rec);
		return true;
	}

	public function url($url)
	{
		header("Location: ".$url);
	}
}
?>